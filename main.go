package main

import (
	"errors"
	"fmt"
	"github.com/docopt/docopt-go"
	"market-basket/register"
	"os"
	"strings"
	"text/tabwriter"
)

var usage = `Market Register: Basket.

Usage:
  basket <product-code>...
  basket --version

Options:
  -h --help     Show this screen.
  --version     Show version.`

func parseOptions() docopt.Opts {
	argv := os.Args[1:]
	opts, err := docopt.ParseArgs(usage, argv, "1.2.3")
	panicOnError(err)
	return opts
}

func createClient() register.Client {
	cfg := Config{
		GetEnv: func(key string) string {
			return os.Getenv(key)
		},
	}

	apiUrl := cfg.GetApiUrl()
	return register.Client{BaseUrl: apiUrl}
}

func startRegister(client register.Client) (*register.Register, error) {
	productData, err := client.FetchProducts()
	if err != nil {
		return nil, errors.New("failed to fetch products")
	}

	dealData, err := client.FetchDeals()
	if err != nil {
		return nil, errors.New("failed to fetch deals")
	}

	var dealProcessors []register.DealProcessor
	for _, d := range dealData {
		dealProcessors = append(dealProcessors, register.GenerateDealProcessor(d))
	}

	r := register.NewRegister(productData, dealProcessors)
	return r, nil
}

func printFormattedReceipt(lineItems []register.LineItem) {
	w := tabwriter.NewWriter(os.Stdout, 8, 8, 0, ' ', 0)
	defer w.Flush()

	headerFooterFormat := "%s\t%s\t%v\t\n"
	lineFormat := "%s\t%s\t%.2f\t\n"

	fmt.Fprintf(w, headerFooterFormat, "Item", "", "Price")
	fmt.Fprintf(w, headerFooterFormat, "--------", "", "--------")

	var total float64

	for _, i := range lineItems {
		itemAmount := i.Amount
		switch i.Type {
		case register.ProductItem:
			fmt.Fprintf(w, lineFormat, i.Code, "", itemAmount)
		case register.DiscountItem:
			fmt.Fprintf(w, lineFormat, "", i.Code, i.Amount)
		}
		total += itemAmount
	}

	fmt.Fprintf(w, headerFooterFormat, "--------", "--------", "--------")
	fmt.Fprintf(w, lineFormat, "", "Total", total)

}

func main() {
	opts := parseOptions()

	codes, ok := opts["<product-code>"].([]string)
	if !ok {
		panic(fmt.Sprintf("Error parsing options: %v", opts))
	}

	client := createClient()
	r, err := startRegister(client)
	panicOnError(err)

	b := r.Baskets[0]

	for _, c := range codes {
		err = b.AddProduct(r.Products, strings.ToUpper(c))
		if err != nil {
			panic(fmt.Sprintf("Error adding product [%s] to basket", c))
		}
	}

	receipt := r.Receipt(b)
	printFormattedReceipt(receipt)
}

func panicOnError(err error) {
	if err != nil {
		panic(err)
	}
}
