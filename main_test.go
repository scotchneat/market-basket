package main

//
//import (
//	"market-basket/register"
//	"net/http"
//	"net/http/httptest"
//	"net/url"
//	"reflect"
//	"testing"
//)
//
//const (
//	ValidProductResponseData = `[{"code": "CH1", "description": "Chai", "priceInCents": 311},
//							{"code": "AP1", "description": "Apples", "priceInCents": 600}]`
//	ValidDealsResponseData = `[{"code":"BOGO","buyProduct":"CF1","getProduct":"CF1","buyQuantity":1,"getQuantity":1,"discountType":1,"value":100,"max":-1},
//							{"code":"APPL","buyProduct":"AP1","getProduct":"AP1","buyQuantity":3,"getQuantity":-1,"discountType":2,"value":450,"max":-1}]`
//)
//
//func TestStartRegister(t *testing.T) {
//	tests := []struct {
//		name         string
//		responseJson string
//		expected     *register.Register
//		wantErr      bool
//	}{
//		{},
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//				w.WriteHeader(http.StatusOK)
//				w.Header().Set("content-type", "application/json")
//				w.Write([]byte(tt.responseJson))
//			}))
//			tsUrl, _ := url.Parse(ts.URL)
//
//			apiClient := register.Client{BaseUrl: tsUrl}
//			got, err := startRegister(apiClient)
//			if (err != nil) != tt.wantErr {
//				t.Errorf("startRegister() error = %v, wantErr %v", err, tt.wantErr)
//				return
//			}
//			if !reflect.DeepEqual(got, tt.expected) {
//				t.Errorf("startRegister() = %v, expected %v", got, tt.expected)
//			}
//		})
//	}
//}
