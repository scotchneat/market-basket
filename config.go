package main

import (
	"fmt"
	"net/url"
)

const (
	EnvPrefix     = "MARKET"
	ApiEnvVar     = "API"
	DefaultApiUrl = `http://localhost:8080/`
)

type Config struct {
	GetEnv func(key string) string
}

func (c *Config) GetApiUrl() string {
	apiUrlValue := c.GetEnv(fmt.Sprintf("%s_%s", EnvPrefix, ApiEnvVar))
	if len(apiUrlValue) == 0 {
		apiUrlValue = DefaultApiUrl
	}

	apiUrl, err := url.Parse(apiUrlValue)
	if err != nil {
		panic(err)
	}
	return apiUrl.String()
}
