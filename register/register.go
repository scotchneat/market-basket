package register

type Register struct {
	Products       Products
	DealProcessors []DealProcessor
	Baskets        []Basket
}

func NewRegister(products Products, dealProcessor []DealProcessor) *Register {
	baskets := []Basket{
		NewBasket(),
	}
	return &Register{Products: products, DealProcessors: dealProcessor, Baskets: baskets}
}

//
//func NewRegister(client api.Client) (*Register, error) {
//	products, err := client.FetchProducts()
//	if err != nil {
//		return nil, errors.New("[REGISTER] failed to fetch products")
//	}
//
//	deals, err := client.FetchDeals()
//	if err != nil {
//		return nil, errors.New("[REGISTER] failed to fetch deals")
//	}
//	return &Register{Products: products, Deals: deals}, nil
//}

type LineItem struct {
	Type   LineItemType
	Code   string
	Amount float64
}

func (r *Register) Receipt(b Basket) []LineItem {
	var items []LineItem
	for _, p := range b.items {
		items = append(items, LineItem{
			Type:   ProductItem,
			Code:   p.Code,
			Amount: float64(p.PriceInCents) / 100,
		})
	}

	var appliedDeals []AppliedDeal
	for _, d := range r.DealProcessors {
		d := d.Process(items)
		appliedDeals = append(appliedDeals, d...)
	}

	var receipt []LineItem
	for n, item := range items {
		receipt = append(receipt, item)
		for _, d := range appliedDeals {
			if d.LineItem == n {
				dealItem := LineItem{
					Type:   DiscountItem,
					Code:   d.Code,
					Amount: d.Amount,
				}
				receipt = append(receipt, dealItem)
			}
		}
	}
	return receipt
}
