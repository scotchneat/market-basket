package register

import (
	"fmt"
	"time"
)

type LineItemType int

const (
	ProductItem LineItemType = iota + 1
	DiscountItem
)

type Basket struct {
	StartTime time.Time
	EndTime   time.Time
	items     Products
}

func NewBasket() Basket {
	var prods Products
	return Basket{StartTime: time.Now(), items: prods}
}

func (b *Basket) AddProduct(products Products, code string) error {
	p, err := products.Get(code)
	if err != nil {
		return fmt.Errorf("product [%s] not found", code)
	}
	b.items = append(b.items, p)
	return nil
}
