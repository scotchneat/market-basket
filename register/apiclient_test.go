package register

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"reflect"
	"testing"
)

const (
	ValidProductResponseData = `[{"code": "CH1", "description": "Chai", "priceInCents": 311},
							{"code": "AP1", "description": "Apples", "priceInCents": 600}]`
	ValidDealsResponseData = `[{"code":"BOGO","buyProduct":"CF1","getProduct":"CF1","buyQuantity":1,"getQuantity":1,"discountType":1,"value":100,"max":-1},
							{"code":"APPL","buyProduct":"AP1","getProduct":"AP1","buyQuantity":3,"getQuantity":-1,"discountType":2,"value":450,"max":-1}]`
)

type handlerDetails struct {
	path         string
	httpStatus   int
	responseJson string
}

func generateTestServer(hd []handlerDetails) (*httptest.Server, *url.URL) {
	mux := http.NewServeMux()
	for _, d := range hd {
		mux.Handle(d.path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(d.httpStatus)
			w.Header().Set("content-type", "application/json")
			w.Write([]byte(d.responseJson))
		}))
	}
	ts := httptest.NewServer(mux)
	tsUrl, _ := url.Parse(ts.URL)
	return ts, tsUrl
}

func TestGetProducts(t *testing.T) {
	tests := []struct {
		name         string
		responseJson string
		wantProducts []Product
		wantErr      bool
	}{
		{"Valid payload", ValidProductResponseData,
			[]Product{
				{"CH1", "Chai", 311},
				{"AP1", "Apples", 600},
			},
			false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				w.Header().Set("content-type", "application/json")
				w.Write([]byte(tt.responseJson))
			}))
			tsUrl, _ := url.Parse(ts.URL)

			api := Client{BaseUrl: tsUrl.String()}
			gotProducts, err := api.FetchProducts()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetProducts() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotProducts, tt.wantProducts) {
				t.Errorf("GetProducts() = %v, want %v", gotProducts, tt.wantProducts)
			}
		})
	}
}

func TestGetDeals(t *testing.T) {
	tests := []struct {
		name         string
		responseJson string
		wantDeals    []Deal
		wantErr      bool
	}{
		{"Valid payload", ValidDealsResponseData,
			[]Deal{
				{"BOGO", "CF1", "CF1", 1, 1, PercentDiscount, 100, -1},
				{"APPL", "AP1", "AP1", 3, -1, AmountDiscount, 450, -1},
			},
			false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				w.Header().Set("content-type", "application/json")
				w.Write([]byte(tt.responseJson))
			}))
			tsUrl, _ := url.Parse(ts.URL)

			api := Client{BaseUrl: tsUrl.String()}
			gotDeals, err := api.FetchDeals()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetDeals() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotDeals, tt.wantDeals) {
				t.Errorf("GetDeals() = %v, want %v", gotDeals, tt.wantDeals)
			}
		})
	}
}
