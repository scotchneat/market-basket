package register

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
)

type DiscountType int

const (
	PercentDiscount DiscountType = iota + 1
	AmountDiscount
)

type Product struct {
	Code         string `json:"code"`
	Description  string `json:"description"`
	PriceInCents int    `json:"priceInCents"`
}

type Deal struct {
	Code         string       `json:"code"`
	BuyProduct   string       `json:"buyProduct"`
	GetProduct   string       `json:"getProduct"`
	BuyQuantity  int          `json:"buyQuantity"`
	GetQuantity  int          `json:"getQuantity"`
	DiscountType DiscountType `json:"discountType"`
	Value        int          `json:"value"`
	Max          int          `json:"max"`
}

//Client fetches data from backend API
type Client struct {
	BaseUrl string
}

func (c *Client) makeRequest(resourcePath string, v interface{}) error {
	u, err := url.Parse(c.BaseUrl)
	panicOnError(err)

	u.Path = path.Join(u.Path, resourcePath)
	urlString := u.String()
	r, err := http.Get(urlString)
	panicOnError(err)
	defer r.Body.Close()

	switch r.StatusCode {
	case http.StatusOK:
		bodyBytes, err := ioutil.ReadAll(r.Body)
		panicOnError(err)

		err = json.Unmarshal(bodyBytes, &v)
		panicOnError(err)

		return nil
	case http.StatusNotFound:
		return errors.New("resource not found")
	default:
		panic(err)
	}

}

func (c *Client) FetchProducts() (products []Product, err error) {
	err = c.makeRequest("/products", &products)
	return
}

func (c *Client) FetchDeals() (deals []Deal, err error) {
	err = c.makeRequest("/deals", &deals)
	return
}

func panicOnError(err error) {
	if err != nil {
		panic(err)
	}
}
