package register

import (
	"reflect"
	"testing"
)

var deals = map[string]*Deal{
	"BOGO": {Code: "BOGO", BuyProduct: "CF1", GetProduct: "CF1", BuyQuantity: 1, GetQuantity: 1, DiscountType: PercentDiscount, Value: 100, Max: -1},
	"APPL": {Code: "APPL", BuyProduct: "AP1", GetProduct: "AP1", BuyQuantity: 3, GetQuantity: -1, DiscountType: AmountDiscount, Value: 150, Max: -1},
	"CHMK": {Code: "CHMK", BuyProduct: "CH1", GetProduct: "MK1", BuyQuantity: 1, GetQuantity: 1, DiscountType: PercentDiscount, Value: 100, Max: 1},
	"APOM": {Code: "APOM", BuyProduct: "OM1", GetProduct: "AP1", BuyQuantity: 1, GetQuantity: 1, DiscountType: PercentDiscount, Value: 50, Max: -1},
}

func TestPriceDropProcessor_Process(t *testing.T) {

	tests := []struct {
		name          string
		dealProcessor DealProcessor
		itemList      []LineItem
		expected      []AppliedDeal
	}{
		{"Buy 1. Get 2 at the discount", &PriceDropProcessor{deals["APPL"]},
			[]LineItem{
				{ProductItem, "AP1", 600},
				{ProductItem, "AP1", 600},
				{ProductItem, "AP1", 600}},
			[]AppliedDeal{
				{0, deals["APPL"].Code, -1.5},
				{1, deals["APPL"].Code, -1.5},
				{2, deals["APPL"].Code, -1.5}},
		},
		{"No matches", &PriceDropProcessor{deals["APPL"]}, []LineItem{{}}, []AppliedDeal{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			if gotAppliedDeals := tt.dealProcessor.Process(tt.itemList); !reflect.DeepEqual(gotAppliedDeals, tt.expected) {
				t.Errorf("PriceDropProcessor.Process() = %v, want %v", gotAppliedDeals, tt.expected)
			}
		})
	}
}

func Test_getMatchedIndexes(t *testing.T) {
	tests := []struct {
		name     string
		l        []LineItem
		code     string
		expected []int
	}{
		{"Match one", []LineItem{
			{ProductItem, "one", 600},
			{ProductItem, "two", 600}},
			"one", []int{0}},
		{"Match all", []LineItem{
			{ProductItem, "one", 600},
			{ProductItem, "one", 600}},
			"one", []int{0, 1}},
		{"Match first and last", []LineItem{
			{ProductItem, "one", 600},
			{ProductItem, "two", 600},
			{ProductItem, "two", 600},
			{ProductItem, "one", 600}},
			"one", []int{0, 3}},
		{"No matches", []LineItem{
			{ProductItem, "none", 600}},
			"no", []int{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getMatchedIndexes(tt.l, tt.code); !reflect.DeepEqual(got, tt.expected) {
				t.Errorf("ItemList.getMatchedIndexes(): %v, expected: %v", got, tt.expected)
			}
		})
	}
}

func TestGenerateDealProcessor(t *testing.T) {
	tests := []struct {
		name string
		deal Deal
		want DealProcessor
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GenerateDealProcessor(tt.deal); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GenerateDealProcessor() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBuyGetProcessor_Process(t *testing.T) {
	tests := []struct {
		name     string
		Deal     *Deal
		items    []LineItem
		expected []AppliedDeal
	}{
		{"BOGO Coffee", deals["BOGO"], []LineItem{
			{ProductItem, "CF1", 11.23},
			{ProductItem, "CF1", 11.23},
			{ProductItem, "CF1", 11.23}},
			[]AppliedDeal{
				{1, deals["BOGO"].Code, -11.23},
			}},
		{"BOGO Coffee - multi", deals["BOGO"], []LineItem{
			{ProductItem, "CF1", 11.23},
			{ProductItem, "CF1", 11.23},
			{ProductItem, "CF1", 11.23},
			{ProductItem, "CF1", 11.23}},
			[]AppliedDeal{
				{1, deals["BOGO"].Code, -11.23},
				{3, deals["BOGO"].Code, -11.23},
			}},
		{"BOGO - MK1 CF1 CF1 CF1", deals["BOGO"], []LineItem{
			{ProductItem, "MK1", 11.23},
			{ProductItem, "CF1", 11.23},
			{ProductItem, "CF1", 11.23},
			{ProductItem, "CF1", 11.23}},
			[]AppliedDeal{
				{2, deals["BOGO"].Code, -11.23},
			}},
		{"simple", deals["CHMK"], []LineItem{
			{ProductItem, "CH1", 3.11},
			{ProductItem, "MK1", 4.75},
			{ProductItem, "MK1", 4.75}},
			[]AppliedDeal{
				{1, deals["CHMK"].Code, -4.75},
			}},
		{"get item later in the list", deals["CHMK"], []LineItem{
			{ProductItem, "CF1", 6.00},
			{ProductItem, "CH1", 6.00},
			{ProductItem, "AP1", 6.00},
			{ProductItem, "MK1", 6.00}},
			[]AppliedDeal{
				{3, deals["CHMK"].Code, -6.00},
			}},
		{"Buy and get items out of order", deals["CHMK"], []LineItem{
			{ProductItem, "CF1", 6.00},
			{ProductItem, "MK1", 6.00},
			{ProductItem, "AP1", 6.00},
			{ProductItem, "CH1", 6.00}},
			[]AppliedDeal{
				{1, deals["CHMK"].Code, -6.00},
			}},
		{"Buy and get 50% off - limit 1", deals["APOM"], []LineItem{
			{ProductItem, "OM1", 6.00},
			{ProductItem, "AP1", 6.00},
			{ProductItem, "AP1", 6.00}},
			[]AppliedDeal{
				{1, deals["APOM"].Code, -3.00},
			}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			bg := BuyGetProcessor{
				Deal: tt.Deal,
			}
			if got := bg.Process(tt.items); !reflect.DeepEqual(got, tt.expected) {
				t.Errorf("BuyGetProcessor.Process() = %v, want %v", got, tt.expected)
			}
		})
	}
}
