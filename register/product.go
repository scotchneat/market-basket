package register

import (
	"fmt"
)

type Products []Product

func (products Products) Get(code string) (Product, error) {
	for _, p := range products {
		if p.Code == code {
			return p, nil
		}
	}
	return Product{}, fmt.Errorf("product [%s] does not exist", code)
}

func (products Products) Codes() []string {
	var codes []string
	for _, p := range products {
		codes = append(codes, p.Code)
	}
	return codes
}
