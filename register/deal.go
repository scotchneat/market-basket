package register

type AppliedDeal struct {
	LineItem int
	Code     string
	Amount   float64
}

func getMatchedIndexes(l []LineItem, code string) []int {
	matched := []int{}
	for i, v := range l {
		if v.Code == code {
			matched = append(matched, i)
		}
	}
	return matched
}

type DealProcessor interface {
	Process(items []LineItem) (appliedDeals []AppliedDeal)
}

func GenerateDealProcessor(deal Deal) DealProcessor {
	if deal.GetQuantity == -1 {
		return &PriceDropProcessor{&deal}
	}
	return &BuyGetProcessor{&deal}
}

type PriceDropProcessor struct {
	*Deal
}

func (p PriceDropProcessor) Process(items []LineItem) []AppliedDeal {
	appliedDeals := []AppliedDeal{}

	matches := getMatchedIndexes(items, p.BuyProduct)
	if len(matches) == 0 || len(matches) < p.BuyQuantity {
		return []AppliedDeal{}
	}

	var processLimit int
	if p.Max == -1 || p.Max > len(items) {
		processLimit = len(items) - 1
	} else {
		processLimit = p.Max
	}

	for i := 0; i < len(matches) && i <= processLimit; i++ {
		itemAmount := items[i].Amount / 100
		dealValue := float64(p.Value) / 100

		var discount float64
		switch p.DiscountType {
		case AmountDiscount:
			discount = dealValue * -1
		case PercentDiscount:
			discount = itemAmount * (dealValue / 100) * -1
		}
		appliedDeals = append(appliedDeals, AppliedDeal{LineItem: matches[i], Code: p.Code, Amount: discount})
	}

	return appliedDeals
}

type BuyGetProcessor struct {
	*Deal
}

func (bg BuyGetProcessor) Process(items []LineItem) []AppliedDeal {
	appliedDeals := []AppliedDeal{}

	buys := getMatchedIndexes(items, bg.BuyProduct)
	if len(buys) == 0 || len(buys) < bg.BuyQuantity {
		return []AppliedDeal{}
	}

	gets := getMatchedIndexes(items, bg.GetProduct)

	var processLimit int
	if bg.Max == -1 || bg.Max > len(items) {
		processLimit = len(items) - 1
	} else {
		processLimit = bg.Max
	}

	var processed []int

ProcessBuys:
	for b := bg.BuyQuantity; b <= len(buys) && b <= processLimit; b += bg.BuyQuantity {
		for _, p := range processed {
			if p == buys[b-1] {
				continue ProcessBuys
			}
		}
		processed = append(processed, buys[b-1])
		currentAppliedDeals := []AppliedDeal{}

	ProcessGets:
		for g := 0; g < len(gets) && len(currentAppliedDeals) < bg.GetQuantity; g++ {
			//alreadyProcessed := false
			for _, p := range processed {
				if p == gets[g] {
					continue ProcessGets
				}
			}

			itemAmount := float64(items[gets[g]].Amount)
			dealValue := float64(bg.Value)

			var discount float64
			switch bg.DiscountType {
			case AmountDiscount:
				discount = (dealValue / 100.0) * -1
			case PercentDiscount:
				discount = itemAmount * (dealValue / 100) * -1
			}

			currentAppliedDeals = append(currentAppliedDeals, AppliedDeal{gets[g], bg.Code, discount})
			processed = append(processed, gets[g])
		}
		appliedDeals = append(appliedDeals, currentAppliedDeals...)
	}

	return appliedDeals
}
