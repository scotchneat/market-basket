FROM golang
ENV GO111MODULE=on

RUN mkdir /market-basket
ADD . /market-basket/

WORKDIR /market-basket
RUN go build -o /register .

ENTRYPOINT ["/register"]

CMD []