MARKET_API=https://gist.githubusercontent.com/scotchneat/ac79f10ef070fdb9bd7d1d0009e1d34c/raw/e2678524de92107a128ceac1bf017f83536ceeca/
BUILD_OUTPUT_PATH?=./bin
TEST_OUTPUT_PATH?=.
BUILD_COMMAND=GARCH=amd64 go build
CONTAINER_REGISTRY?=scotchneat/market-basket
IMAGE_TAG?=latest

help:  ## Prints this help message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-25s\033[0m %s\n", $$1, $$2}'

build: ## Compiles the application for the current OS and architecture
	go build .

build-all: build-darwin build-linux build-windows ## Builds the application for all OS on amd64 architecture

build-darwin: ## Builds the application for a MACOS on amd64 architecture
	echo $(BUILD_OUTPUT_PATH) && \
	GOOS=darwin $(BUILD_COMMAND) -v -o $(BUILD_OUTPUT_PATH)/market-basket_darwin_amd64 .

build-linux: ## Builds the application for a linux OS on amd64 architecture
	GOOS=linux $(BUILD_COMMAND) -v -o $(BUILD_OUTPUT_PATH)/market-basket_linux_amd64 .

build-windows: ## Builds the application for a Windows OS on amd64 architecture
	GOOS=windows $(BUILD_COMMAND)  -v -o $(BUILD_OUTPUT_PATH)/market-basket_win_amd64 .

run: ## Compiles and runs the application (expects products as PRODUCT="CODE CODE")
	MARKET_API=$(MARKET_API) go run . $(PRODUCTS)

test: ## Runs GO TEST on application packages
	go test -v -cover ./... | tee $(TEST_OUTPUT_PATH)/test-results.txt

docker-build: ## Builds the docker image of the application
	docker build . -t $(CONTAINER_REGISTRY):$(IMAGE_TAG)

docker-run: ## Runs the latest version of the application image (expects products as PRODUCT="CODE CODE")
	docker run -it --rm -e MARKET_API=$(MARKET_API) $(CONTAINER_REGISTRY):$(IMAGE_TAG) $(PRODUCTS)

docker-build-run: docker docker-run ## Builds the latest container image and runs the application