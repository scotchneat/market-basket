package main

import (
	"reflect"
	"testing"
)

func TestConfig_getApiUrl(t *testing.T) {
	type fields struct {
		getEnv func(key string) string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{"Default", fields{getEnv: func(key string) string { return "" }}, "http://localhost:8080/"},
		{"Custom", fields{getEnv: func(key string) string { return "https://somerehost:8888/" }}, "https://somerehost:8888/"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Config{
				GetEnv: tt.fields.getEnv,
			}
			if got := c.GetApiUrl(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Config.getApiUrl().String() = %v, expected %v", got, tt.want)
			}
		})
	}
}
