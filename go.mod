module market-basket

go 1.12

require (
	github.com/docopt/docopt-go v0.0.0-20180111231733-ee0de3bc6815
	github.com/google/pprof v0.0.0-20190723021845-34ac40c74b70
	github.com/ianlancetaylor/demangle v0.0.0-20181102032728-5e5cf60278f6 // indirect
	golang.org/x/arch v0.0.0-20190312162104-788fe5ffcd8c
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/tools v0.0.0-20190808195139-e713427fea3f
)
