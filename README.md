# market-basket

Solution for ObjectRocket's [code challenge](https://gist.github.com/jbartels/f780f548d935866340e9afe963c40c1f)

# Usage

 The code can be run from a docker container by running the following commands:
 
 ```bash
# First set the API environment variable
export MARKET_API=https://gist.githubusercontent.com/scotchneat/ac79f10ef070fdb9bd7d1d0009e1d34c/raw/44f90742c6db89bd0b3eea04797249f4c9a4a650/
# Then run the docker container
docker run -it --rm -e MARKET_API=$MARKET_API registry.gitlab.com/scotchneat/market-basket:latest om1 ap1
# Where `ch1 mk1` can be set to a list of product codes
```

> Note a list of product codes can be found [here](https://gist.githubusercontent.com/scotchneat/ac79f10ef070fdb9bd7d1d0009e1d34c/raw/44f90742c6db89bd0b3eea04797249f4c9a4a650/products)  
> And a list of deals details can be found [here](https://gist.githubusercontent.com/scotchneat/ac79f10ef070fdb9bd7d1d0009e1d34c/raw/44f90742c6db89bd0b3eea04797249f4c9a4a650/deals)

The above command should output the following:

```bash
Item            Price
--------        --------
OM1             3.69
AP1             6.00
        APOM    -3.00
------------------------
        Total   6.69
```

Try changing the list of product codes. Run the following command:  
`docker run -it --rm -e MARKET_API=$MARKET_API registry.gitlab.com/scotchneat/market-basket:latest ap1 ap1 ap1`  

And you should see the following output:

```bash
Item            Price
--------        --------
AP1             6.00
        APPL    -1.50
AP1             6.00
        APPL    -1.50
AP1             6.00
        APPL    -1.50
------------------------
        Total   13.50
```